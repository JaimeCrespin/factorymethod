//
//  ViewController.swift
//  FactoryMethod
//
//  Created by USRDEL on 4/22/19.
//  Copyright © 2019 USRDEL. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
    }
    @IBAction func botonRojo(_ sender: Any) {
        view.backgroundColor = primario
    }
    
    @IBAction func botonVerde(_ sender: Any) {
        view.backgroundColor = secundario
    }
    
}

protocol FabricaColores {
    func create() -> UIColor
}

class FabricaColorPrimario: FabricaColores {
    func create() -> UIColor {
        return .red
    }
}

class FabricaColorSecundario: FabricaColores {
    func create() -> UIColor {
        return .green
    }
}

let fabricaColorRojo = FabricaColorPrimario()
let fabricaColorVerde = FabricaColorSecundario()
let primario = fabricaColorRojo.create()
let secundario = fabricaColorVerde.create()
